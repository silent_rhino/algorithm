#pragma once

#define list_node_pos(T) listNode<T>*

template <typename T>
struct listNode {
    T data;
    list_node_pos(T) pred;
    list_node_pos(T) succ;

    listNode() {}
    listNode(T data_in, list_node_pos(T) pred_in = nullptr, list_node_pos(T) succ_in = nullptr)
            : data(data_in),pred(pred_in), succ(succ_in)
    {}
    void linkTo(listNode<T> &next_node);
    list_node_pos(T) insert_as_pred(const T &e);
    list_node_pos(T) insert_as_succ(const T &e);
};