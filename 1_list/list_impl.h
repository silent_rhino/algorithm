#include <iostream>
#include "list.h"

using namespace std;

template <typename T>
void list<T>::init() { 
    header = new listNode<T>; 
    trailer = new listNode<T>;
    header->succ = trailer;
    header->pred = nullptr;
    trailer->pred = header;
    trailer->succ = nullptr;
    size = 0; 
}

template <typename T>
list<T>::~list() {
    clear();
    delete header;
    delete trailer;
}

template <typename T>
void list<T>::clear() {
    auto loop_time = size;
    while(loop_time > 0) {
        loop_time--;
        remove(header->succ);
    }
}

template <typename T>
T list<T>::remove(list_node_pos(T) pos_to_remove) {
    if (pos_to_remove == header || pos_to_remove == trailer) {
        return 0;
    }
    T remove_data = pos_to_remove->data;
    size--;
    pos_to_remove->pred->succ = pos_to_remove->succ;
    pos_to_remove->succ->pred = pos_to_remove->pred;
    delete pos_to_remove;
    return remove_data;
}

template <typename T>
void list<T>::show() {
    if (size == 0) {
        cout << "this list have no node." << endl;
        return;
    }
    list_node_pos(T) current_node = first();
    while(current_node != trailer->pred) {
        cout << current_node->data << " -> ";
        current_node = current_node->succ;
    }
    cout << current_node->data << endl;
    
    return;
}

template <typename T>
list_node_pos(T) list<T>::insert_as_first(const T &elem) {
    size++;
    return header->insert_as_succ(elem);
}

template <typename T>
list_node_pos(T) list<T>::insert_as_last(const T &elem) {
    size++;
    return trailer->insert_as_pred(elem);
} 

/*****
 * @brief: pos -> elem
 * ****/
template <typename T>
list_node_pos(T) list<T>::insert_after(list_node_pos(T) pos, const T &elem) {
    if (pos == nullptr) {
        return nullptr;
    }
    size++;
    return pos->insert_as_succ(elem);
} 

/*****
 * @brief: elem -> pos
 * ****/
template <typename T>
list_node_pos(T) list<T>::insert_before(list_node_pos(T) pos, const T &elem) {
    if (pos == nullptr) {
        return nullptr;
    }
    size++;
    return pos->insert_as_pred(elem);
} 

template <typename T>
list_node_pos(T) list<T>::operator[](int r) {
    if (r < 0) {
        cout << "[error] rank is invalid." << endl;
        return nullptr;
    }
    list_node_pos(T) current_node = first();
    for(; r > 0; r--) {
        current_node = current_node->succ;
    }
    return current_node;
}

/*****
 * @brief: find elem value in [. . ., pos)
 * @return: 1. not found in n steps
 *          2. find range out of header
 * ****/
template <typename T>
list_node_pos(T) list<T>::find(const T &elem, int n, list_node_pos(T) pos) {
    while(0 < n) {
        if (pos == header) {
            return nullptr;
        }
        pos = pos->pred;
        if (elem == pos->data) {
            return pos;
        }
        n--;
    }
    return nullptr;
}

/*****
 * @brief: deduplicate for unordered lsit
 * ****/
template <typename T>
int list<T>::deduplicate() {
   int old_size = size;
   list_node_pos(T) to_find = first();
   list_node_pos(T) finded = nullptr;
   for (int r = 0; to_find != trailer;) {
        finded ? remove(finded) : r++;
        to_find = to_find->succ;
        finded = find(to_find->data, r, to_find);
    }
   return old_size - size;
}

/*****
 * @brief: uniquify for ordered lsit
 * ****/
template <typename T>
int list<T>::uniquify() {
    if (size < 2) {
        return 0;
    }
    int old_size = size;
    list_node_pos(T) first_node = first();
    list_node_pos(T) second_node = first_node->succ;
    while (trailer != second_node) {
        if (first_node->data != second_node->data) {
            first_node = second_node;
        } else {
            remove(second_node);
        }
        second_node = first_node->succ;
    }
    return old_size - size;
}

/*****
 * @brief: reverse the list
 * ****/
template <typename T>
void list<T>::reverse() { 
   list_node_pos(T) reverse_head = header;
   list_node_pos(T) reverse_tail = trailer;
   for (int i = 1; i < size; i += 2) {
        reverse_head = reverse_head->succ;
        reverse_tail = reverse_tail->pred;
        swap(reverse_head->data, reverse_tail->data); 
   }
}

/*****
 * @brief: merge 2 list like : [first_pos, ...] and [second_pos, ...]
 * ****/
template <typename T> 
void list<T>::merge(list_node_pos(T) &first_pos,
                    int first_len,
                    list<T> &list,
                    list_node_pos(T) &second_pos,
                    int second_len) {
   list_node_pos(T) restore = first_pos->pred;
    while (0 < second_len) { // second_len must > first_len
        if ((0 < first_len) && (first_pos->data <= second_pos->data)) {
            if (second_pos == ( first_pos = first_pos->succ)) {
                break;
            }
            first_len--;
        } else {
            second_pos = second_pos->succ;
            insert_before(first_pos, list.remove(second_pos->pred));
            second_len--;
        }
    }
    first_pos = restore->succ;
}

/*****
 * @brief: merge sort
 * ****/
template <typename T> 
void list<T>::merge_sort(list_node_pos(T) &pos, int num) {
    if (num < 2) {
        return;
    }
    int first_len = num / 2; 
    list_node_pos(T) second_pos = pos;
    for (int i = 0; i < first_len; i++) {
       second_pos = second_pos->succ;
    }
    merge_sort(pos, first_len);
    merge_sort(second_pos, num - first_len); 
    merge(pos, first_len, *this, second_pos, num - first_len); 
}

/*****
 * @brief: select max elem after pos with n step
 * ****/
template <typename T> 
list_node_pos(T) list<T>::select_max(list_node_pos(T) pos, int n) {
   list_node_pos(T) max = pos;
    for (list_node_pos(T) cur = pos; 1 < n; n--) {
        cur = cur->succ;
        if (cur->data > max->data) {
            max = cur; 
        } 
    }
    return max; 
}

/*****
 * @brief: select sort
 * ****/
template <typename T> 
void list<T>::select_sort(list_node_pos(T) pos, int n) {
    list_node_pos(T) head = pos->pred;
    list_node_pos(T) tail = pos;
    for (int i = 0; i < n; i++) {
        tail = tail->succ; 
    }
    while (1 < n) { 
        list_node_pos(T) max = select_max(head->succ, n); 
        insert_before(tail,remove(max));
        tail = tail->pred;
        n--;
    }
}

template <typename T>
list_node_pos(T) list<T>::search(const T &elem, int n, list_node_pos(T) pos) {
    while(0 < n--) {
        pos = pos->pred;
        if (pos->data <= elem) {
            return pos;
        }
    }
    return nullptr;
}

template <typename T> 
void list<T>::insertion_sort(list_node_pos(T) pos, int n) {
    for (int r = 0; r < n; r++) {
        insert_after(search(pos->data, r, pos), pos->data);
        pos = pos->succ;
        remove(pos->pred);
    }
}

template <typename T>
void list<T>::inner_sort(list_node_pos(T) pos, int n, sort_mode mode) {
    switch (mode)
    {
        case MERGE_SORT:
            std::cout << "merge sort" << std::endl;
            merge_sort(pos, n);
            break;

        case INSERTION_SORT:
            std::cout << "insertion sort" << std::endl;
            insertion_sort(pos, n);
            break;

        case SELECTION_SORT:
            std::cout << "selection sort" << std::endl;
            select_sort(pos, n);
            break;

        default:
            std::cout << "choose sort mode plz" << std::endl;
            break;
    }
}