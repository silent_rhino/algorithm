#pragma once
#include "list_node_impl.h"

enum sort_mode {
   MERGE_SORT,
   INSERTION_SORT,
   SELECTION_SORT
};

template <typename T>
class list {
protected:
    void init();
    void clear();
    void merge(list_node_pos(T) &fst_pos, int fst_len, list<T> &list,
               list_node_pos(T) &sed_pos, int sed_len);
    void merge_sort(list_node_pos(T) &pos, int num);
    list_node_pos(T) select_max(list_node_pos(T) pos, int n);
    void select_sort(list_node_pos(T) pos, int n);
    void insertion_sort(list_node_pos(T) pos, int n);
    void inner_sort(list_node_pos(T) pos, int n, sort_mode mode);

public:
    list() {init();} 
    ~list(); 
    void show();
    int deduplicate();
    int uniquify();
    int getSize() {return size;}
    void reverse();
    bool empty() {return (size == 0);}
    T remove(list_node_pos(T) pos_to_remove);
    list_node_pos(T)find(const T &elem, int n, list_node_pos(T) pos);
    list_node_pos(T)search(const T &elem, int n, list_node_pos(T) pos);
    list_node_pos(T)insert_as_first(const T &elem);
    list_node_pos(T)insert_as_last(const T &elem);
    list_node_pos(T)insert_after(list_node_pos(T)pos, const T &elem);
    list_node_pos(T)insert_before(list_node_pos(T)pos, const T &elem);
    list_node_pos(T)operator[](int r);
    list_node_pos(T)first() const {return header->succ;}
    list_node_pos(T)last() const {return trailer->pred;}
    void sort(sort_mode mode) {inner_sort(first(), size, mode);}
private:
    int size;
    list_node_pos(T) header;
    list_node_pos(T) trailer;
};