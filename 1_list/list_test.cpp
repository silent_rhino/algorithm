#include <iostream>
#include "list_impl.h"

using namespace std;

int main() {
    list<float> list;
    auto p = list.insert_as_first(1.0);
    p = list.insert_after(p, 2.0);
    p = list.insert_after(p, 2.0);
    p = list.insert_after(p, 2.0);
    p = list.insert_after(p, 3.0);
    p = list.insert_after(p, 4.0);
    p = list.insert_after(p, 5.0);
    p = list.insert_after(p, 6.0);
    list.show();
    list.uniquify();
    list.show();
    list.reverse();
    list.show();
    sort_mode mode = MERGE_SORT;
    list.sort(mode);
    list.show();
}