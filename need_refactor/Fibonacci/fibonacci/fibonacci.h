#pragma once

class fib { 
private:
   int fst_number;
   int sed_number;
public:
    fib(int n) {
        n += 2;
        fst_number = 0;
        sed_number = 1;
        while(0 < n--) {
            fst_number += sed_number;
            sed_number = fst_number - sed_number;
        }
    } 
    int get(){return sed_number;}
};