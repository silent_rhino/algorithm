#include "avl_tree.h"

template <typename T>
binNodePos(T) AVLTree<T>::insert(T const& e)
{
    binNodePos(T) insert_pos = this->search(e);
    if (nullptr != insert_pos) {
        std::cout << "the date you want insert is already in the tree " << std::endl;
        return nullptr;
    }

    if (e < this->hot->data_) {
        insert_pos = this->insertAsLeftChild(this->hot, e);
    } else {
        insert_pos = this->insertAsRightChild(this->hot, e);
    }

    for (binNodePos(T) current_pos = insert_pos->parent_; nullptr != current_pos; current_pos = current_pos->parent_) {
        if (!AvlBalanced(*current_pos)) {
            if (HasParent(*current_pos)) {
                auto parent = current_pos->parent_;
                if (IsLeftChild(*current_pos)) {
                    parent->left_child_ = rotateAt(current_pos);
                    parent->left_child_->parent_ = parent;
                } else {
                    parent->right_child_ = rotateAt(current_pos);
                    parent->right_child_->parent_ = parent;
                }
            } else {
                this->root_ = rotateAt(current_pos);
            }
            // original hole tree is balanced, insertion only affects one node
            // because height before rotate equal after rotate
            break;
        } else {
            this->updateHeight(current_pos);
        }
    }
    return insert_pos;
}

template <typename T>
binNodePos(T)& AVLTree<T>::rotateAt(binNodePos(T) &c)
{
    binNodePos(T) b = tallerChild(*c);
    binNodePos(T) a = tallerChild(*b);
    if (IsLeftChild(*b) && IsLeftChild(*a)) {
        std::cout << "zig-zig" << std::endl;
        auto t1 = a->left_child_;
        auto t2 = a->right_child_;
        auto t3 = b->right_child_;
        auto t4 = c->right_child_;
        return connect34(a, b, c, t1, t2, t3, t4);
    } else if(IsLeftChild(*b) && IsRightChild(*a)) {
        std::cout << "zig-zag" << std::endl;
        auto t1 = b->left_child_;
        auto t2 = a->left_child_;
        auto t3 = a->right_child_;
        auto t4 = c->right_child_;
        return connect34(b, a, c, t1, t2, t3, t4);
    } else if(IsRightChild(*b) && IsRightChild(*a)) {
        std::cout << "zag-zag" << std::endl;
        auto t1 = c->left_child_;
        auto t2 = b->left_child_;
        auto t3 = a->left_child_;
        auto t4 = a->right_child_;
        return connect34(c, b, a, t1, t2, t3, t4);
    } else if(IsRightChild(*b) && IsLeftChild(*a)) {
        std::cout << "zag-zig" << std::endl;
        auto t1 = c->left_child_;
        auto t2 = a->left_child_;
        auto t3 = a->right_child_;
        auto t4 = b->right_child_;
        return connect34(c, a, b, t1, t2, t3, t4);
    }
}

template <typename T>
binNodePos(T)& AVLTree<T>::connect34(binNodePos(T) &min,
                                     binNodePos(T) &mid,
                                     binNodePos(T) &max,
                                     binNodePos(T) &t1,
                                     binNodePos(T) &t2,
                                     binNodePos(T) &t3,
                                     binNodePos(T) &t4)
{
    min->left_child_ = t1;
    if (nullptr != t1) {
        t1->parent_ = min;
    }
    min->right_child_ = t2;
    if (nullptr != t2) {
        t2->parent_ = min;
    }
    this->updateHeight(min);

    max->left_child_ = t3;
    if (nullptr != t3) {
        t3->parent_ = max;
    }
    max->right_child_ = t4;
    if (nullptr != t4) {
        t4->parent_ = max;
    }
    this->updateHeight(max);

    mid->left_child_ = min;
    min->parent_ = mid;
    mid->right_child_ = max;
    max->parent_ = mid;
    this->updateHeight(mid);
    mid->parent_ = nullptr;
    return mid;
}

template <typename T>
void AVLTree<T>::remove(T const& e)
{
    binNodePos(T) remove_pos = this->search(e);
    if (nullptr == remove_pos) {
        std::cout << "the date you want remove is not in the tree " << std::endl;
        return;
    }
    this->removeIn(remove_pos);
    for (binNodePos(T) current_pos = this->hot; nullptr != current_pos; current_pos = current_pos->parent_) {
        if (!AvlBalanced(*current_pos)) {
            if (HasParent(*current_pos)) {
                auto parent = current_pos->parent_;
                if (IsLeftChild(*current_pos)) {
                    parent->left_child_ = rotateAt(current_pos);
                    parent->left_child_->parent_ = parent;
                } else {
                    parent->right_child_ = rotateAt(current_pos);
                    parent->right_child_->parent_ = parent;
                }
            } else {
                this->root_ = rotateAt(current_pos);
            }
        } else {
            this->updateHeight(current_pos);
        }
    }
}