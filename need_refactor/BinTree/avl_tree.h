#include "bin_search_tree_impl.h"

template <typename T>
class AVLTree : public binSearchTree<T>
{
    public:
        AVLTree(const char* str) : binSearchTree<T>(str) {}
        virtual binNodePos(T) insert(T const& e) override;
        virtual void remove(T const& e) override;
    private:
        binNodePos(T)& rotateAt(binNodePos(T) &c);
        binNodePos(T)& connect34(binNodePos(T)& min, binNodePos(T)& mid, binNodePos(T)& max,
                                 binNodePos(T)& t1, binNodePos(T)& t2, binNodePos(T)& t3, binNodePos(T)& t4);
};