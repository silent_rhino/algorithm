#include "splay_tree.h"

template <typename T>
binNodePos(T) SplayTree<T>::splay(binNodePos(T) splay_node)
{
    if (splay_node == nullptr) {
        return nullptr;
    }
    if (splay_node == this->root_) {
        std::cout << "node has already root, so no need to splay" << std::endl;
        return splay_node;
    }
    binNodePos(T) parent_node;
    binNodePos(T) grand_node;
    while((parent_node = splay_node->parent_) && (grand_node = parent_node->parent_)) {
        binNodePos(T) grand_parent = grand_node->parent_;
        if (nullptr == grand_parent) {
            this->root_ = splayAt(splay_node, parent_node, grand_node);
            #ifdef DEBUG
                std::string preorder_string;
                std::string inorder_string;
                this->PreOrderTraverse(preorder_string, this->root_);
                this->InOrderTraverse(inorder_string, this->root_);
                std::cout << "splay tree pre order: " << preorder_string << std::endl;
                std::cout << "splay tree in order: " << inorder_string << std::endl;
            #endif
            return this->root_;
        } else {
            if (IsLeftChild(*grand_node)) {
                grand_parent->left_child_ = splayAt(splay_node, parent_node, grand_node);
                grand_parent->left_child_->parent_ = grand_parent;
                #ifdef DEBUG
                    std::string preorder_string;
                    std::string inorder_string;
                    this->PreOrderTraverse(preorder_string, grand_parent->left_child_);
                    this->InOrderTraverse(inorder_string, grand_parent->left_child_);
                    std::cout << "splay tree pre order: " << preorder_string << std::endl;
                    std::cout << "splay tree in order: " << inorder_string << std::endl;
                #endif

            } else {
                grand_parent->right_child_ = splayAt(splay_node, parent_node, grand_node);
                grand_parent->right_child_->parent_ = grand_parent;
                
                #ifdef DEBUG
                    std::string preorder_string;
                    std::string inorder_string;
                    this->PreOrderTraverse(preorder_string, grand_parent->right_child_);
                    this->InOrderTraverse(inorder_string, grand_parent->right_child_);
                    std::cout << "splay tree pre order: " << preorder_string << std::endl;
                    std::cout << "splay tree in order: " << inorder_string << std::endl;
                #endif
            }
            this->updateHeightAbove(grand_parent);
        }
    }

    if (parent_node = splay_node->parent_) {
        binNodePos(T) new_root = splayAtRoot(splay_node, parent_node);
        #ifdef DEBUG
            std::string preorder_string;
            std::string inorder_string;
            this->PreOrderTraverse(preorder_string, new_root);
            this->InOrderTraverse(inorder_string, new_root);
            std::cout << "splay tree pre order: " << preorder_string << std::endl;
            std::cout << "splay tree in order: " << inorder_string << std::endl;
        #endif
        this->root_ = new_root;
        return new_root;
    }
}

template <typename T>
binNodePos(T) SplayTree<T>::splayAt(binNodePos(T) splay_node,
                                    binNodePos(T) parent_node,
                                    binNodePos(T) grand_node)
{
    if (IsLeftChild(*splay_node) && IsLeftChild(*parent_node)) {
        auto t1 = splay_node->left_child_;
        auto t2 = splay_node->right_child_;
        auto t3 = parent_node->right_child_;
        auto t4 = grand_node->right_child_;

        splay_node->left_child_ = t1;
        if (nullptr != t1) {
            t1->parent_ = splay_node;
        }
        splay_node->right_child_ = parent_node;
        splay_node->parent_ = nullptr;

        parent_node->left_child_ = t2;
        if (nullptr != t2) {
            t2->parent_ = parent_node;
        }
        parent_node->right_child_ = grand_node;
        parent_node->parent_ = splay_node;

        grand_node->left_child_ = t3;
        grand_node->right_child_ = t4;
        if (nullptr != t3) {
            t3->parent_ = grand_node;
        }
        if (nullptr != t4) {
            t4->parent_ = grand_node;
        }
        grand_node->parent_ = parent_node;
        this->updateHeightAbove(grand_node);
        return splay_node;
    } else if(IsLeftChild(*splay_node) && IsRightChild(*parent_node)) {
        auto t1 = grand_node->left_child_;
        auto t2 = splay_node->left_child_;
        auto t3 = splay_node->right_child_;
        auto t4 = parent_node->right_child_;

        grand_node->left_child_ = t1;
        if (nullptr != t1) {
            t1->parent_ = grand_node;
        }
        grand_node->right_child_ = t2;
        if (nullptr != t2) {
            t2->parent_ = grand_node;
        }
        grand_node->parent_ = splay_node;
        this->updateHeight(grand_node);

        parent_node->left_child_ = t3;
        if (nullptr != t3) {
            t3->parent_ = parent_node;
        }
        parent_node->right_child_ = t4;
        if (nullptr != t4) {
            t4->parent_ = parent_node;
        }
        parent_node->parent_ = splay_node;
        this->updateHeight(parent_node);

        splay_node->left_child_ = grand_node;
        splay_node->right_child_ = parent_node;
        splay_node->parent_ = nullptr;
        this->updateHeightAbove(splay_node);
        return splay_node;

    } else if(IsRightChild(*splay_node) && IsRightChild(*parent_node)) {
        auto t1 = grand_node->left_child_;
        auto t2 = parent_node->left_child_;
        auto t3 = splay_node->left_child_;
        auto t4 = splay_node->right_child_;

        grand_node->left_child_ = t1;
        if (nullptr != t1) {
            t1->parent_ = grand_node;
        }
        grand_node->right_child_ = t2;
        if (nullptr != t2) {
            t2->parent_ = grand_node;
        }
        grand_node->parent_ = parent_node;
        
        parent_node->left_child_ = grand_node;
        parent_node->right_child_ = t3;
        if (nullptr != t3) {
            t3->parent_ = parent_node;
        }
        parent_node->parent_ = splay_node;

        splay_node->left_child_ = parent_node;
        splay_node->right_child_ = t4;
        if (nullptr != t4) {
            t4->parent_ = splay_node;
        }
        splay_node->parent_ = nullptr;
        this->updateHeightAbove(grand_node);
        return splay_node;
    } else if(IsRightChild(*splay_node) && IsLeftChild(*parent_node)) {
        auto t1 = parent_node->left_child_;
        auto t2 = splay_node->left_child_;
        auto t3 = splay_node->right_child_;
        auto t4 = grand_node->right_child_;

        parent_node->left_child_ = t1;
        if (nullptr != t1) {
            t1->parent_ = parent_node;
        }
        parent_node->right_child_ = t2;
        if (nullptr != t2) {
            t2->parent_ = parent_node;
        }
        parent_node->parent_ = splay_node;
        this->updateHeight(parent_node);

        grand_node->left_child_ = t3;
        if (nullptr != t3) {
            t3->parent_ = grand_node;
        }
        grand_node->right_child_ = t4;
        if (nullptr != t4) {
            t4->parent_ = grand_node;
        }
        grand_node->parent_ = splay_node;
        this->updateHeight(grand_node);

        splay_node->left_child_ = parent_node;
        splay_node->right_child_ = grand_node;
        splay_node->parent_ = nullptr;
        this->updateHeightAbove(splay_node);
        return splay_node;
    }
}

template <typename T>
binNodePos(T) SplayTree<T>::splayAtRoot(binNodePos(T) splay_node,
                                        binNodePos(T) parent_node)
{
    if (IsLeftChild(*splay_node)) {
        auto t1 = splay_node->left_child_;
        auto t2 = splay_node->right_child_;
        auto t3 = parent_node->right_child_;

        splay_node->left_child_ = t1;
        if (nullptr != t1) {
            t1->parent_ = splay_node;
        }
        splay_node->right_child_ = parent_node;
        splay_node->parent_ = nullptr;

        parent_node->left_child_ = t2;
        if (nullptr != t2) {
            t2->parent_ = parent_node;
        }
        parent_node->right_child_ = t3;
        if (nullptr != t3) {
            t3->parent_ = parent_node;
        }
        parent_node->parent_ = splay_node;
        this->updateHeightAbove(parent_node);
        return splay_node;
    } else {
        auto t1 = parent_node->left_child_;
        auto t2 = splay_node->left_child_;
        auto t3 = splay_node->right_child_;

        parent_node->left_child_ = t1;
        if (nullptr != t1) {
            t1->parent_ = parent_node;
        }
        parent_node->right_child_ = t2;
        if (nullptr != t2) {
            t2->parent_ = parent_node;
        }
        parent_node->parent_ = splay_node;

        splay_node->left_child_ = parent_node;
        splay_node->right_child_ = t3;
        if (nullptr != t3) {
            t3->parent_ = splay_node;
        }
        splay_node->parent_ = nullptr;
        this->updateHeightAbove(parent_node);
        return splay_node;
    }
}

template <typename T>
binNodePos(T) SplayTree<T>::search(T const& e)
{
    this->hot = nullptr;
    binNodePos(T) root_node = this->root_;
    binNodePos(T) splay_node = this->searchIn(root_node, this->hot, e);
    return splay(splay_node);
}

template <typename T>
binNodePos(T) SplayTree<T>::insert(T const& e)
{
    binNodePos(T) insert_pos = search(e);
    if (nullptr != insert_pos) {
        std::cout << "the date you want insert is already in the tree " << std::endl;
        return nullptr;
    }
    if (e < this->hot->data_) {
        return splay(this->insertAsLeftChild(this->hot, e));
    } else {
        return splay(this->insertAsRightChild(this->hot, e));
    }
}

template <typename T>
void SplayTree<T>::remove(T const& e)
{
    this->hot = nullptr;
    binNodePos(T) root_node = this->root_;
    binNodePos(T) remove_pos = this->searchIn(root_node, this->hot, e);
    if (nullptr == remove_pos) {
        std::cout << "the date you want remove is not in the tree " << std::endl;
        return;
    }
    this->removeIn(remove_pos);
    splay(this->hot);
}