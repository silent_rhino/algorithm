#include "bin_search_tree_impl.h"

template <typename T>
class SplayTree : public binSearchTree<T>
{
    public:
        SplayTree(const char* str) : binSearchTree<T>(str) {}
        virtual binNodePos(T) search(T const& e) override;
        virtual binNodePos(T) insert(T const& e) override;
        virtual void remove(T const& e) override;
    // private:

        binNodePos(T) splay(binNodePos(T) splay_pos);
        binNodePos(T) splayAt(binNodePos(T) splay_node, binNodePos(T) parent_node, binNodePos(T) grand_node);
        binNodePos(T) splayAtRoot(binNodePos(T) splay_node, binNodePos(T) parent_node);
};