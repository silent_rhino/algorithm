#pragma once

#include <vector>
#define bTreeNodePos(T) bTreeNode<T>*
template <typename T>
struct bTreeNode {
    int size_;
    bTreeNodePos(T) parent;
    std::vector<T> key;
    std::vector<bTreeNodePos(T)> child;
    bTreeNode() {
        size_ = 0;
        parent = nullptr;
        std::vector<int>::iterator child_iter = child.begin();
        child.insert(child_iter, size_, nullptr);
    }
    bTreeNode(T const &e, int size, bTreeNodePos(T) left_child_, bTreeNodePos(T) right_child_) {
        size_ = size;
        parent = nullptr;
        typename std::vector<T>::iterator key_iter; 
        typename std::vector<bTreeNodePos(T)>::iterator child_iter;
        key_iter = key.begin();
        child_iter= child.begin();
        key.insert(key_iter, e);
        child.insert(child_iter, size_, nullptr);
        if (nullptr != left_child_) {
            child[0] = left_child_;
        }
        if (nullptr != right_child_) {
            child[1] = right_child_;
        }
    }
};