#include <string>
#include "bin_tree_impl.h"

template <typename T>
class binSearchTree : public binTree<T>
{
    public:
        binNodePos(T) hot;
    public:
        binSearchTree() : binTree<T>() {}
        binSearchTree(const char* str) : binTree<T>() {
            createTreeFromStr(str);
        }
        virtual binNodePos(T) search(T const& e);
        virtual binNodePos(T) insert(T const& e);
        virtual void remove(T const& e);
        void showHot() {
            if (nullptr != hot) {
                std::cout << "hot is " << hot->data_ << std::endl;
            } else {
                std::cout << "hot is nullptr" << std::endl;                
            }
        }
    protected:
        void createTreeFromStr(const char* str);
        binNodePos(T) searchIn(binNodePos(T) search_node, binNodePos(T)& hot, T const& e);
        void removeIn(binNodePos(T) &remove_pos);
};
