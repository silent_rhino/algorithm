/******************************************************************************************
 * Data Structures in C++
 * ISBN: 7-302-33064-6 & 7-302-33065-3 & 7-302-29652-2 & 7-302-26883-3
 * Junhui DENG, deng@tsinghua.edu.cn
 * Computer Science & Technology, Tsinghua University
 * Copyright (c) 2003-2019. All rights reserved.
 ******************************************************************************************/

/*DSA*/#include "RPN.h"

float evaluate ( char* S, char*& RPN ) { 
   Stack<float> opnd; Stack<char> optr; 
   char* expr = S;
   optr.push ( '\0' ); 
   while ( !optr.empty() ) { 
      if ( isdigit ( *S ) ) { 
         readNumber ( S, opnd ); append ( RPN, opnd.top() ); 
      } else 
         switch ( orderBetween ( optr.top(), *S ) ) { 
            case '<': 
               optr.push ( *S ); S++; 
               break;
            case '=': 
               optr.pop(); S++; 
               break;
            case '>': { 
               char op = optr.pop(); append ( RPN, op ); 
               if ( '!' == op ) { 
                  float pOpnd = opnd.pop(); 
                  opnd.push ( calcu ( op, pOpnd ) ); 
               } else { 
                  float pOpnd2 = opnd.pop(), pOpnd1 = opnd.pop(); 
                  opnd.push ( calcu ( pOpnd1, op, pOpnd2 ) ); 
               }
               break;
            }
            default : exit ( -1 ); 
         }
      displayProgress ( expr, S, opnd, optr, RPN );
   }
   return opnd.pop(); 
}