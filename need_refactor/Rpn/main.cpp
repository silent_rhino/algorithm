
#include <iostream>
#include <ctype.h>
using namespace std;

void readNumber(char* p, float &stk) { 
    stk = ((float)(*p - '0'));
    while(isdigit(*(++p))) {
        stk =  stk * 10 + ( *p - '0' );
    }
    if ('.' != *p) {
        return;
    }
    float fraction = 1; 
    while(isdigit(*(++p))) {
        stk = (stk + (*p - '0') * (fraction /= 10)); 
    }
}

int main() {
    char in[20] = "1234.562";
    float a;
    readNumber(in, a);
    cout << a << endl;
}
