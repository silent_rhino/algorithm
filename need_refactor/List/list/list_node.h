#pragma once

typedef int Rank;
#define listNodePos(T) listNode<T>*

template <typename T>
struct listNode {
    T data;
    listNodePos(T) pred;
    listNodePos(T) succ;

    listNode() {}
    listNode(T data_in, listNodePos(T) pred_in = nullptr, listNodePos(T) succ_in = nullptr)
            : data(data_in),pred(pred_in), succ(succ_in)
    {}
    void linkTo(listNode<T> &next_node);
    listNodePos(T) insertAsPred(const T &e);
    listNodePos(T) insertAsSucc(const T &e);
};