#include "list_node.h"

template <typename T>
void listNode<T>::linkTo(listNode<T> &next_node) {
    succ = &next_node;
    next_node.pred = this;
}

template <typename T>
listNodePos(T) listNode<T>::insertAsPred(const T &e) {
    listNodePos(T) insert_pred = new listNode<T> (e, pred, this);
    pred->succ = insert_pred;
    pred = insert_pred;
    return insert_pred;
}

template <typename T>
listNodePos(T) listNode<T>::insertAsSucc(const T &e) {
    listNodePos(T) insert_succ = new listNode<T> (e, this, succ);
    succ->pred = insert_succ;
    succ = insert_succ;
    return insert_succ;
}