#pragma once
#include "list_node.h"

enum sort_mode {
   MERGE_SORT,
   INSERTION_SORT,
   SELECTION_SORT
};

template <typename T>
class list {
protected:
    void init();
    void clear();
    void merge(listNodePos(T) &fst_pos, int fst_len, list<T> &list,
               listNodePos(T) &sed_pos, int sed_len);
    void mergeSort(listNodePos(T) &pos, int num);
    listNodePos(T) selectMax(listNodePos(T) pos, int n);
    void selectionSort(listNodePos(T) pos, int n);
    void insertionSort(listNodePos(T) pos, int n);
    void innerSort(listNodePos(T) pos, int n, sort_mode mode);

public:
    list() {init();} 
    ~list(); 
    void show();
    int deduplicate(); // unordered
    int uniquify();  // ordered
    int getSize() {return size;}
    void reverse();
    bool empty() {return (size == 0);}
    T remove(listNodePos(T) pos_to_remove);
    listNodePos(T)find(const T &elem, int n, listNodePos(T) pos);
    listNodePos(T)search(const T &elem, int n, listNodePos(T) pos);
    listNodePos(T)insertAsFirst(const T &elem);
    listNodePos(T)insertAsLast(const T &elem);
    listNodePos(T)insertAfter(listNodePos(T)pos, const T &elem);
    listNodePos(T)insertBefore(listNodePos(T)pos, const T &elem);
    listNodePos(T)operator[](Rank r);
    listNodePos(T)first() const {return header->succ;}
    listNodePos(T)last() const {return trailer->pred;}
    void sort(sort_mode mode) {innerSort(first(), size, mode);}
private:
    int size;
    listNodePos(T) header;
    listNodePos(T) trailer;
};