#include <iostream>
#include "list.h"

using namespace std;

template <typename T>
void list<T>::init() { 
    header = new listNode<T>; 
    trailer = new listNode<T>;
    header->succ = trailer;
    header->pred = nullptr;
    trailer->pred = header;
    trailer->succ = nullptr;
    size = 0; 
}

template <typename T>
list<T>::~list() {
    clear();
    delete header;
    delete trailer;
}

template <typename T>
void list<T>::clear() {
    auto loop_time = size;
    while(loop_time > 0) {
        loop_time--;
        remove(header->succ);
    }
}

template <typename T>
T list<T>::remove(listNodePos(T) pos_to_remove) {
    // if (pos_to_remove == header || pos_to_remove == trailer) {
    //     return 0;
    // }
    T remove_data = pos_to_remove->data;
    size--;
    pos_to_remove->pred->succ = pos_to_remove->succ;
    pos_to_remove->succ->pred = pos_to_remove->pred;
    delete pos_to_remove;
    return remove_data;
}

template <typename T>
void list<T>::show() {
    if (size == 0) {
        cout << "this list have no node." << endl;
        return;
    }
    listNodePos(T) current_node = first();
    while(current_node != trailer->pred) {
        cout << current_node->data << " -> ";
        current_node = current_node->succ;
    }
    cout << current_node->data << endl;
    
    return;
}

template <typename T>
listNodePos(T) list<T>::insertAsFirst(const T &elem) {
    size++;
    return header->insertAsSucc(elem);
}

template <typename T>
listNodePos(T) list<T>::insertAsLast(const T &elem) {
    size++;
    return trailer->insertAsPred(elem);
} 

template <typename T>
listNodePos(T) list<T>::insertAfter(listNodePos(T) pos, const T &elem) {
    if (pos == nullptr) {
        return nullptr;
    }
    size++;
    return pos->insertAsSucc(elem);
} 

template <typename T>
listNodePos(T) list<T>::insertBefore(listNodePos(T) pos, const T &elem) {
    size++;
    return pos->insertAsPred(elem);
} 


template <typename T>
listNodePos(T) list<T>::operator[](Rank r) {
    if (r < 0) {
        cout << "[error] rank is invalid." << endl;
        return nullptr;
    }
    listNodePos(T) current_node = first();
    for(; r > 0; r--) {
        current_node = current_node->succ;
    }
    return current_node;
}

template <typename T>
listNodePos(T) list<T>::find(const T &elem, int n, listNodePos(T) pos) {
    while(0 < n--) {
        if (elem == (pos = pos->pred)->data) {
            return pos;
        }
    }
    return nullptr;
}

template <typename T>
int list<T>::deduplicate() {
   int oldSize = size;
   listNodePos(T) to_find = first();
   listNodePos(T) finded = nullptr;
   for (Rank r = 0; to_find != trailer;
        to_find = to_find->succ, finded = find(to_find->data, r, to_find))
        finded ? remove(finded) : r++;
   return oldSize - size;
}

template <typename T>
int list<T>::uniquify() {
    if (size < 2) {
        return 0;
    }
    int oldSize = size;
    listNodePos(T) first_node = first();
    listNodePos(T) second_node = nullptr;
    while (trailer != (second_node = first_node->succ)) {
        if (first_node->data != second_node->data) {
            first_node = second_node;
        } else {
            remove(second_node);
        }
    }
    return oldSize - size;
}

template <typename T>
void list<T>::reverse() { 
   listNodePos(T) reverse_head = header;
   listNodePos(T) reverse_tail = trailer;
   for (int i = 1; i < size; i += 2) {
      swap((reverse_head = reverse_head->succ)->data,
            (reverse_tail = reverse_tail->pred)->data); 
   }
}

template <typename T> 
void list<T>::merge(listNodePos(T) &fst_pos, int fst_len, list<T> &list,
                    listNodePos(T) &sed_pos, int sed_len) {
   listNodePos(T) restore = fst_pos->pred;
    while (0 < sed_len) {
        if ((0 < fst_len) && (fst_pos->data <= sed_pos->data)) {
            if (sed_pos == ( fst_pos = fst_pos->succ)) {
                break;
            }
            fst_len--;
        } else {
            insertBefore(fst_pos, list.remove((sed_pos = sed_pos->succ)->pred));
            sed_len--;
        }
    }
    fst_pos = restore->succ;
}

template <typename T> 
void list<T>::mergeSort(listNodePos(T) &pos, int num) {
    if (num < 2) {
        return;
    }
    int fst_len = num / 2; 
    listNodePos(T) sed_pos = pos;
    for (int i = 0; i < fst_len; i++) {
       sed_pos = sed_pos->succ;
    }
    mergeSort(pos, fst_len); mergeSort(sed_pos, num - fst_len); 
    merge(pos, fst_len, *this, sed_pos, num - fst_len); 
}

template <typename T> 
listNodePos(T) list<T>::selectMax(listNodePos(T) pos, int n) {
   listNodePos(T) max = pos;
    for (listNodePos(T) cur = pos; 1 < n; n--) {
        cur = cur->succ;
        if (cur->data > max->data) {
            max = cur; 
        } 
    }
    return max; 
}

template <typename T> 
void list<T>::selectionSort(listNodePos(T) pos, int n) {
    listNodePos(T) head = pos->pred;
    listNodePos(T) tail = pos;
    for (int i = 0; i < n; i++) {
        tail = tail->succ; 
    }
    while (1 < n) { 
        listNodePos(T) max = selectMax(head->succ, n); 
        insertBefore(tail,remove(max));
        tail = tail->pred;
        n--;
    }
}

template <typename T>
listNodePos(T) list<T>::search(const T &elem, int n, listNodePos(T) pos) {
    while(0 < n--) {
        pos = pos->pred;
        if (pos->data <= elem) {
            return pos;
        }
    }
    return nullptr;
}

template <typename T> 
void list<T>::insertionSort(listNodePos(T) pos, int n) {
    for (int r = 0; r < n; r++) {
        insertAfter(search(pos->data, r, pos), pos->data);
        pos = pos->succ;
        remove(pos->pred);
    }
}

template <typename T>
void list<T>::innerSort(listNodePos(T) pos, int n, sort_mode mode) {
    switch (mode)
    {
        case MERGE_SORT:
            std::cout << "merge sort" << std::endl;
            mergeSort(pos, n);
            break;

        case INSERTION_SORT:
            std::cout << "insertion sort" << std::endl;
            insertionSort(pos, n);
            break;

        case SELECTION_SORT:
            std::cout << "selection sort" << std::endl;
            selectionSort(pos, n);
            break;

        default:
            std::cout << "choose sort mode plz" << std::endl;
            break;
    }
}