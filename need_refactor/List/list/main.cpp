#include <iostream>
#include "list_node.h"
#include "list_node.cpp"
#include "list.h"
#include "list.cpp"

using namespace std;

int main() {
    list<float> list_0;
    list_0.insertAsFirst(3.0);
    list_0.insertAsFirst(3.0);
    list_0.insertAsFirst(2.0);
    list_0.insertAsFirst(1.0);
    list_0.insertAsFirst(2.0);
    list_0.insertAsFirst(1.0);
    list_0.insertAsFirst(8.0);
    list_0.insertAsFirst(9.0);
    list_0.insertAsFirst(10.0);
    list_0.insertAsFirst(3.0);
    list_0.insertAsLast(4.0);
    list_0.insertAsLast(4.0);
    list_0.insertAsFirst(1.0);
    list_0.show();
    sort_mode mode = MERGE_SORT;
    list_0.sort(mode);
    list_0.show();
    
    cout << "---------------" << endl;
    cout << "2020-05-02 done" << endl;
    cout << "---------------" << endl;
}