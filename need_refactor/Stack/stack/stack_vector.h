#pragma once
#include "vector.h" 

template <typename T>
class stack: public vector<T> {
public: 
   void push(const T &elem) {vector<T>::insert(elem);}
   T pop() {return vector<T>::remove(vector<T>::size() - 1);}
   T& top() {return (*this)[vector<T>::size() - 1];}
};
