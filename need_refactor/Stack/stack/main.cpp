#ifdef STACK_LIST
#include "stack_list.h"
#include "list.cpp"
#include "list_node.cpp"
#elif STACK_VECTOR
#include "stack_vector.h"
#include "vector.cpp"
#endif

using namespace std;
int main() {
    stack<int> stack_0;
    stack_0.push(1);
    stack_0.push(2);
    stack_0.push(3);
    stack_0.push(4);
    stack_0.push(5);
    stack_0.show();
    stack_0.pop();
    stack_0.pop();
    stack_0.pop();
    stack_0.show();
    cout << stack_0.top() << endl;
    cout << "2020-05-03 done." << endl;

}
