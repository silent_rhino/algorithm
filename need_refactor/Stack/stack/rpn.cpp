#include <ctype.h>
#include <math.h>
#include <stdlib.h>
#include <cstring>
#include <string>
#ifdef STACK_LIST
#include "stack_list.h"
#include "list.cpp"
#include "list_node.cpp"
#endif

using namespace std;

#define OPERATOR_NUM 9
typedef enum { ADD, SUB, MUL, DIV, POW, FAC, L_P, R_P, EOE} Operator;

const char priority[OPERATOR_NUM][OPERATOR_NUM] = {
   /*          +      -      *      /      ^      !      (      )      \0 */
   /* + */    '>',   '>',   '<',   '<',   '<',   '<',   '<',   '>',   '>',
   /* - */    '>',   '>',   '<',   '<',   '<',   '<',   '<',   '>',   '>',
   /* * */    '>',   '>',   '>',   '>',   '<',   '<',   '<',   '>',   '>',
   /* / */    '>',   '>',   '>',   '>',   '<',   '<',   '<',   '>',   '>',
   /* ^ */    '>',   '>',   '>',   '>',   '>',   '<',   '<',   '>',   '>',
   /* ! */    '>',   '>',   '>',   '>',   '>',   '>',   ' ',   '>',   '>',
   /* ( */    '<',   '<',   '<',   '<',   '<',   '<',   '<',   '=',   ' ',
   /* ) */    ' ',   ' ',   ' ',   ' ',   ' ',   ' ',   ' ',   ' ',   ' ',
   /* \0 */   '<',   '<',   '<',   '<',   '<',   '<',   '<',   ' ',   '='
};

Operator GetOperatorRank (char op) { 
   switch ( op ) {
      case '+' : return ADD; 
      case '-' : return SUB; 
      case '*' : return MUL; 
      case '/' : return DIV; 
      case '^' : return POW; 
      case '!' : return FAC; 
      case '(' : return L_P; 
      case ')' : return R_P; 
      case '\0': return EOE; 
      default  : exit(-1); 
   }
}

char OperatorOrder(char op1, char op2) {
    return priority[GetOperatorRank(op1)][GetOperatorRank(op2)];
}

float fact(int n) {
    int i;
    float result;
    result = 1;
    for(i = 1; i <= n; i++) {
        result = result * i;
    }
    return result;
}

void append(char* &rpn, float number) {
   char buf[64];
   if (number != (float)(int)number) {
      sprintf(buf, "%.2f \0", number);
   } else {
      sprintf(buf, "%d \0", (int)number);
   }  
   rpn = (char*)realloc(rpn, sizeof(char)*(strlen(rpn) + strlen(buf) + 1));
   strcat(rpn, buf);
}

void append(char* &rpn, char opera) {
   int n = strlen(rpn);
   rpn = (char*)realloc(rpn, sizeof(char) * (n + 3));
   sprintf(rpn + n, "%c ", opera);
   rpn[n + 2] = '\0';
}

float calculation(float a, char op, float b) { 
    switch (op) {
        case '+' : return a + b;
        case '-' : return a - b;
        case '*' : return a * b;
        case '/' : 
                    if (0 == b) {
                      exit(-1);
                    }
                    return a/b;
        case '^' : return pow(a, b);
        default  : exit(-1);
    }
}

float calculation(char op, float b) {
    switch (op) {
        case '!' : return(float)fact((int)b);
        default  : exit(-1);
    }
}

void readNumber(char* p, stack<float>& stk) { 
    stk.push((float)(*p - '0'));
    while(isdigit(*(++p))) {
        stk.push(stk.pop() * 10 + ( *p - '0' ));
    }
    if ('.' != *p) {
        return;
    }
    float fraction = 1; 
    while(isdigit(*(++p))) {
        stk.push((stk.pop() + (*p - '0') * (fraction /= 10)));
    }
}

float evaluate(char* expression, char* &rpn_expression) {
   stack<float> number_stack;
   stack<char> operator_stack;
   operator_stack.push('\0');
   while (!operator_stack.empty()) {
      if (isdigit(*expression)) {
         readNumber(expression, number_stack);
         append(rpn_expression, number_stack.top());
         expression++;
      } else {
         switch(OperatorOrder(operator_stack.top(), *expression)) { 
            case '<':
               operator_stack.push(*expression);
               expression++; 
               break;
            case '=': 
               operator_stack.pop();
               expression++; 
               break;
            case '>': {
               char op = operator_stack.pop();
               append(rpn_expression, op);
               if ('!' == op) {
                  float number = number_stack.pop(); 
                  number_stack.push(calculation(op, number)); 
               } else {
                  float second_number = number_stack.pop();
                  float first_number = number_stack.pop(); 
                  number_stack.push(calculation(first_number, op, second_number));
               }
               break;
            }
            default : exit(-1); 
         }
      }
   }
   if (number_stack.empty()) {
      cout << "[error] numbet stack is empty." << endl;
      return 0;
   }
   return number_stack.pop(); 
}

int main(int argc, char* argv[]) {
   for ( int i = 1; i < argc; i++ ) {
      system("clear");
      printf("\nPress any key to evaluate: [%s]\a\n", argv[i]);
      getchar();
      char* rpn_expression = (char*)malloc(sizeof(char) * 1);
      rpn_expression[0] = '\0';
      float value = evaluate(argv[i], rpn_expression);
      printf ( "EXPR\t: %s\n", argv[i]); 
      printf ( "RPN\t: [ %s]\n", rpn_expression);
      printf ( "Value\t= %.1f = %d\n", value, (int)value);
      free(rpn_expression);
      rpn_expression = nullptr;
      getchar();
   }
}