#pragma once
#include "list.h"

template <typename T>
class stack: public list<T> { 
public:
   void push(const T &elem) {list<T>::insertAsLast(elem);} 
   T pop(){return list<T>::remove(list<T>::last());} 
   T& top() {return list<T>::last()->data;} 
};
