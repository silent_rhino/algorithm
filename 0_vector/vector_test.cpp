#include <iostream>
#include "vector_impl.h"

using namespace std;

/*  note :
 *  vector<T>& vec 这个入参值得注意：
 *  如果入参为指针，则本质上是按值传递，被调函数中真正操作的是与入参值相同的形参变量，可以解引用、->调用函数等，
 *  但如果做赋值操作改变不了函数外的实参。
 *  如果入参为引用，会在被调函数的栈中开辟空间存放实参的地址，被调函数对形参的任何操作都被处理成间接寻址，
 *  即通过栈中存放的地址访问主调函数中的实参变量， 被调函数对形参的任何操作都会影响主调函数中的实参变量
 */

/*  note :
 *  重载了 = 操作符，只有在赋值时才会调用，初始化时调用的是构造函数
 */

template <typename T>
void test_vector_ctor(vector<T>& vec, const T* array, uint32_t array_size);
template <typename T>
void test_vector_find(vector<T>& vec);
template <typename T>
void test_vector_sort(vector<T>& vec);

int main() {
    const int32_t array[11] = {3, 5, 9, 23, 45, 56, 78, 89, 56, 89, 99};
    vector<int32_t> vec;
    test_vector_ctor<int32_t>(vec, array, 11);
    test_vector_sort<int32_t>(vec);
    auto elem = vec.linear_select(0, 10, 9);
    std::cout << "9-largest elem : " << elem << std::endl;
}

template <typename T>
void test_vector_ctor(vector<T>& vec, const T* array, uint32_t array_size) {
    bool test_ok = 1;
    vec = vector<T>(array, 0, array_size - 1);   // call ctor and operator=
    for (uint32_t i = 0; i < array_size; i++) {
        if (array[i] != vec[i]) {
            test_ok = 0;
            break;
        }
    }
    std::cout << "vector test ctor : " << (test_ok == 0 ? "fail" : "success") << std::endl;
}

template <typename T>
void test_vector_sort(vector<T>& vec) {
    sort_mode mode[4];
    mode[0] = BUBBLE_SORT;
    mode[1] = MERGE_SORT;
    mode[2] = HEAP_SORT;
    mode[3] = QUICK_SORT;
    for (sort_mode cur_mode : mode) {
        vec.sort(cur_mode);
        vec.show();
    }
}