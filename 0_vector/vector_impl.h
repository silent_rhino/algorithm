
#include <iostream>
#include "vector.h"

template <typename T>
void vector<T>::copyFrom(T const *src, int low, int high) {
    _elem = new T[_capacity = 2 * (high - low)];
    _size = 0;
    while (low <= high) {
        _elem[_size++] = src[low++]; 
    }
}

/**
 * @biref: expand vector capacity if necessary
 *         new capacity = old capacity * 2
 * */
template <typename T>
void vector<T>::expand() {
    if (_size < _capacity) {
        return;
    }
    if (_capacity < DEFAULT_CAPACITY) {
        _capacity = DEFAULT_CAPACITY;
    }
    T *oldElem = _elem;
    _elem = new T[_capacity * 2];
    for (int i = 0; i < _size; ++i) {
        _elem[i] = oldElem[i];
    }
    delete [] oldElem;
}

template <typename T>
void vector<T>::shrink() { 
   if (_capacity < DEFAULT_CAPACITY * 2) {
       return; 
   }
   if ( _size * 4 > _capacity ) {
       return; 
   }
   T* oldElem = _elem;
   _elem = new T[_capacity / 2]; 
   for (int i = 0; i < _size; i++) {
       _elem[i] = oldElem[i]; 
   }
   delete [] oldElem; 
}

template <typename T>
T& vector<T>::operator[](int rank) {
    return _elem[rank];
}

template <typename T>
vector<T> &vector<T>::operator=(const vector<T> &vector) {
    if (_elem) {
        delete [] _elem; 
    }
    copyFrom(vector._elem, 0, vector.size() - 1);
    return *this; 
}

template <typename T>
void vector<T>::show() const {
    cout << "vector :";
    for (int i = 0; i < _size; i++) {
        cout << _elem[i] << " ";
    }
    cout << endl;
}

/***********
 * @brief: find elements in unorder vector
 * @return: -1 failed
 *          other elements index
 * **********/
template <typename T>
int vector<T>::find(T const& elem_to_find, int low, int high) const {
    while (low <= high) {
        if (elem_to_find != _elem[high]) {
            high--;
        } else {
            break;
        }
    }
    return high;
}

template <typename T>
int vector<T>::search(T const& elem_to_search, int low, int high) const {
    while (low < high) {
        int mid = (low + high) / 2;
        (elem_to_search < _elem[mid]) ? (high = mid) : (low = mid + 1);
    }
    return low--;
}

/*******
 * @brief: remove certain element from vector
 * @input: elem_to_remove : index of element to remove
 * @return: value of remove element
 * *****/
template <typename T>
T vector<T>::remove(int elem_to_remove) {
    T elem = _elem[elem_to_remove];
    remove(elem_to_remove, elem_to_remove + 1);
    return elem;
}

/*******
 * @brief: remove element between [low, high) in vector
 * @return: remove elements number
 * *****/
template <typename T>
int vector<T>::remove(int low, int high) {
    if (low == high) {
        return -1;
    }
    while (high < _size) {
        _elem[low++] = _elem[high++];
    }
    _size = low;
    shrink();
    return high - low;
}

/*******
 * @brief: insert element at vector[rank]
 * @return: insert index
 * *****/
template <typename T>
int vector<T>::insert(int rank, T const& elem) {
    expand();
    for (int i = _size; i > rank; i--) {
        _elem[i--] = _elem[i];
    }
    _elem[rank] = elem;
    _size++;
    return rank;
}

template <typename T>
void vector<T>::inner_sort(int low, int high, sort_mode mode) {
    if (low < 0 || high > _size) {
        std::cout << "input para is invalid" << std::endl;
        return;
    }
    switch (mode)
    {
        case BUBBLE_SORT:
            std::cout << "bubble sort, O(n^2)" << std::endl;
            bubble_sort(low, high);
            break;
        case MERGE_SORT:
            std::cout << "merge sort, O(nlogn)" << std::endl;
            merge_sort(low, high);
            break;
        case HEAP_SORT:
            std::cout << "heap sort， O(nlogn)" << std::endl;
            heap_sort(low, high);
            break;
        case QUICK_SORT:
            std::cout << "quick sort， O(nlogn)" << std::endl;
            quick_sort(low, high);
            break;
        default:
            std::cout << "choose sort mode plz" << std::endl;
            break;
    }
}

/*******
 * @brief: bubble sort, it optimizes the case that elements are ordered
 * *****/
template <typename T>
void vector<T>::bubble_sort(int low, int high) {
    uint32_t max_unorder_index = 0;
    for (; low < high; high = max_unorder_index) {
        max_unorder_index = 0;
        for (int i = 0; i < high - low; i++) {
            if (_elem[low + i] > _elem[low + i + 1]) {
                swap(_elem[low + i], _elem[low + i + 1]);
                max_unorder_index = low + i;
            }
        }
    }
}

/*******
 * @brief: merge 2 ordered vector
 *         in detail, vec[low, mid] and vec[mid, high] are both ordered
 * *****/
template <typename T>
void vector<T>::merge(int low, int mid, int high) {
    int first_half_length = mid - low + 1;
    int second_half_length = high - mid; // (high - low + 1) - (first_half_length)

    T first_half[first_half_length];
    for(int i = 0; i < first_half_length; i++) {
        first_half[i] = *(_elem + low + i);
    }
    T *second_half = _elem + low + first_half_length;

    for (int v_step = low, first_index = 0, second_index = 0;
        (first_index < first_half_length) || (second_index < second_half_length);) {
        if (second_index >= second_half_length) {
            _elem[v_step++] = first_half[first_index++];
        } else if (first_index >= first_half_length) {
            _elem[v_step++] = second_half[second_index++];
        } else {
            if (first_half[first_index] <= second_half[second_index]) {
                _elem[v_step++] = first_half[first_index++];
            } else {
                _elem[v_step++] = second_half[second_index++];
            }
        }
    }
}

/*******
 * @brief: merge sort
 * *****/
template <typename T>
void vector<T>::merge_sort(int low, int high) {
    if (low >= high) {
        return;
    }
    int mid = (high + low) / 2;
    merge_sort(low, mid);
    merge_sort(mid + 1, high);
    merge(low,  mid, high);
}

/*******
 * @brief: build full 2-plug heap
 * *****/
template <typename T>
void vector<T>::max_heapify(int low, int high) {
    uint32_t dad = low;
    uint32_t son = dad * 2 + 1;
    // son : _elem[son] and _elem[son + 1](maybe)
    while (son <= high) {
        if (son + 1 <= high && _elem[son] < _elem[son + 1]) {
            son++;
        }
        if (_elem[dad] > _elem[son]) {
            return;
        } else {
            swap(_elem[dad], _elem[son]);
            dad = son;
            son = dad * 2 + 1;
        }
    }
}

/*******
 * @brief: heap sort
 * *****/
template <typename T>
void vector<T>::heap_sort(int low, int high) {
    uint32_t len = high - low + 1;
    // vector[] regard as hierarchical traversal of full 2-plug heap
    // so vec[0, len / 2 - 1] is parent node
    for (int32_t last_dad = len / 2 - 1; last_dad >= 0; last_dad--) {
        max_heapify(last_dad, high);
    }

    for (uint32_t i = high; i > 0; i--) {
        swap(_elem[0], _elem[i]);
        max_heapify(0, i - 1);
    }
}

/*******
 * @brief: partition [low, high], in detail, select _elem[low] as pivot
 *         make all elements smaller than pivot are on the left and
 *         all elements larger than pivot are on the right
 * @notice: although call partition func in quick_sort func is work
 *          but partition can't handle the same elements once, this means that
 *          the same elements will not be adjacent
 * *****/
template <typename T>
int vector<T>::partition(int low, int high) {
    T pivot_value = _elem[low];
    while (low < high) {
        while (low < high && _elem[high] >= pivot_value) {
            --high;
        }
        _elem[low] = _elem[high];
        while (low < high && _elem[low] <= pivot_value) {
            ++low;
        }
        _elem[high] = _elem[low];
    }
    _elem[low] = pivot_value;
    return low;
}

/*******
 * @brief: quick sort
 * *****/
template <typename T>
void vector<T>::quick_sort(int low, int high) {
    if (low < high) {
        uint32_t pivot_index = partition(low, high);\
        quick_sort(low, pivot_index - 1);
        quick_sort(pivot_index + 1, high);
    }
}

/*******
 * @brief: mode : there are more than half of the times in a vector
 * *****/
template <typename T>
T vector<T>::mode(int low, int high) {
    T mode_candidate;
    uint32_t acc = 0;
    for (uint32_t i = low; i < high; i++) {
        if (acc == 0) {
            mode_candidate = _elem[i];
            acc = 1;
        } else {
            _elem[i] == mode_candidate ? acc++ : acc--;
        }
    }
    return mode_candidate;
}

/*******
 * @brief: return k-largest element without sort
 * *****/
template <typename T>
T vector<T>::linear_select(int low, int high, int k) {
    uint32_t size = high - low + 1;
    if (size < 5) {
        this->bubble_sort(low, high);
        return _elem[k - 1];
    }
    for (uint32_t i = 0; i < size/5; i++) {
        uint32_t every_start = low + i * 5;
        uint32_t every_end = every_start + 4;
        this->bubble_sort(every_start, every_end);
        swap(_elem[low + i], _elem[every_start + 2]);
    }
    // handle non align part
    uint32_t non_align_index = low + size/5 * 5;
    uint32_t non_align_size = size - (size/5 * 5);
    this->bubble_sort(non_align_index, high);
    swap(_elem[low + size/5], _elem[non_align_index + (non_align_size == 1 ? 0 : 1)]);

    T median_of_median = linear_select(low, low + size/5, low + size/10 + 1);
    swap(_elem[low], _elem[low + size/10]);
    uint32_t median_of_median_index = partition(low, high);

    if ((median_of_median_index + 1) == k) {
        return _elem[median_of_median_index];
    } else if ((median_of_median_index + 1) < k) {
        linear_select(median_of_median_index + 1, high, k);
    } else if ((median_of_median_index + 1) > k) {
        linear_select(low, median_of_median_index - 1, k);
    }
}

template <typename T>
void vector<T>::unsort(int low, int high) { 
   T* temp = _elem + low; 
   for (int i = high - low; i > 0; i--) {
       swap(temp[i - 1], temp[rand() % i]); 
   }
}

template <typename T>
void vector<T>::deduplicate(int low, int high) {
    int same = low;
    while (low < high) {
        if (_elem[low] != _elem[same]) {
            _elem[same + 1] = _elem[low];
            same++;
        }
        low++;
    }
    int newSize = same + 1;
    remove(newSize, _size);
    show();
}

template <typename T>
void vector<T>::permute(vector<T> &vector) {
   for (int i = vector.size(); i > 0; i-- ) {
       swap(vector[i - 1], vector[rand() % i] ); 
   }
}

