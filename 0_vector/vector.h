/*******************************************************************
 * @features: define Vector structure
 * @date: 2020-04-17
 * @modify_record: 2020-12-10 类模版中的成员函数定义需要写入.h文件
 * @reference: Junhui DENG, Data Structures in C++
*********************************************************************/
#pragma once
#include <iostream>

using namespace std;

enum sort_mode {
   BUBBLE_SORT,
   MERGE_SORT,
   HEAP_SORT,
   QUICK_SORT
};

#define DEFAULT_CAPACITY  5

template <typename T>
class vector {
   protected:
      void copyFrom(T const *src, int low, int high);
      void expand();
      void shrink(); 
      int _size;
      int _capacity;
      T* _elem;

   public:
      vector(int32_t capacity = DEFAULT_CAPACITY, int32_t len = 0, T single_elem = 0) {
         _elem = new T[_capacity = capacity]; 
         for (_size = 0; _size < len; _size++) {
            _elem[_size] = single_elem;
         }
      }
      vector(T const *src, int n) {copyFrom(src, 0, n);} 
      vector(T const *src_vector, int low, int high) {copyFrom(src_vector, low, high);} 
      vector(vector<T> const& src_vector) {copyFrom(src_vector._elem, 0, src_vector._size - 1);} 
      vector(vector<T> const& src_vector, int low, int high) {copyFrom(src_vector._elem, low, high);} 
      ~vector() {delete [] _elem;} 

      T &operator[](int rank); 
      vector<T> &operator=(const vector<T> &vector); 

      void show() const;
      int size() const { return _size; } 
      bool empty() const { return !_size; } 

      // find func for unorder vector
      int find(T const& elem_to_find, int low, int high) const;
      int find(T const& elem_to_find ) const { return find ( elem_to_find, 0, _size - 1); }

      // search func for order vector
      int search(T const& elem_to_search, int lo, int hi ) const; 
      int search(T const& elem_to_search ) const {
         return (_size <= 0) ? -1 : search(elem_to_search, 0, _size);
      }

      T remove(int elem_to_remove); 
      int remove(int low, int high);

      int insert(int rank, T const& elem); 
      int insert(T const& elem) {return insert (_size, elem);} 

      void inner_sort(int low, int high, sort_mode mode);
      void sort(sort_mode mode) {inner_sort(0, _size - 1, mode);}
      void merge(int low, int mid, int high);
      void merge_sort(int low, int high);
      void bubble_sort(int low, int high);
      void max_heapify(int low, int high);
      void heap_sort(int low, int high);
      int partition(int low, int high); 
      void quick_sort(int low, int high);

      T mode(int low, int high);

      T linear_select(int low, int high, int k);

      void unsort(int lo, int hi); 
      void unsort(){unsort(0, _size );} 

      void deduplicate(int low, int high); 

      void permute(vector<T> &vector);

};